from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'book_store.users'
