from django.shortcuts import render, redirect
from django.contrib import messages
from book_store.users.forms import UserRegisterForm


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, ' Account {} has been created! You are now able to log in!'.format(username))
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'store/users/register.html', {'form': form})
