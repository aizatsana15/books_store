from django.contrib import admin

from book_store.apps.store.forms import BookForm
from book_store.apps.store.models import Author, Book


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('pk', 'first_name', 'last_name', )
    search_fields = ('pk', 'first_name', 'last_name',)


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    form = BookForm
    list_display = ('pk', 'title', 'price', 'author_display', 'publish_date', 'dt_created')
    ordering = ('title',)
    filter_horizontal = ('author', )

    def author_display(self, obj):
        return ", ".join([str(author) for author in obj.author.all()])

    author_display.short_description = "Authors"
