from django.views.generic import ListView, CreateView, UpdateView, DetailView, DeleteView

from book_store.apps.store.forms import BookForm
from book_store.apps.store.models import Book, RequestLog
from django.contrib.auth.mixins import LoginRequiredMixin


class BookListView(ListView):
    """
    View displays books list
    """
    model = Book
    template_name = "store/book/list.html"
    content_type = "text/html"
    queryset = Book.objects.all().order_by('title')
    exclude = ('image',)


class BookDetailView(DetailView):
    """
    View displays detail for each book
    """
    model = Book
    template_name = "store/book/book_detail.html"


class BookCreateView(LoginRequiredMixin, CreateView):
    """
    View displays creation form for book
    """
    model = Book
    template_name = "store/book/book_create.html"
    content_type = "text/html"
    fields = '__all__'

    def get_form(self, form_class=None):
        form = super(BookCreateView, self).get_form(form_class)
        # form.fields['date_field'].widget.attrs.update({'class': 'datepicker'})
        return form


class BookUpdateView(LoginRequiredMixin, UpdateView):
    """
    View displays update form for book
    """
    model = Book
    template_name = "store/book/edit.html"
    content_type = "text/html"
    fields = ['image', 'title', 'author', 'isbn', 'price', 'publish_date']


class BookDeleteView(LoginRequiredMixin, DeleteView):
    """
    View is responsible for deleting book from db
    """
    model = Book
    template_name = 'store/book/book_delete.html'
    content_type = "text/html"
    success_url = '/books/'


class RequestLogListView(ListView):
    """
    View displays date from log file in browser
    """
    model = RequestLog
    queryset = RequestLog.objects.all().order_by('time')
    template_name = "store/request/list.html"
    content_type = "text/html"
    context_object_name = 'requests'
    paginate_by = 10

