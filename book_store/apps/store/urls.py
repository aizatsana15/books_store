from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from django.conf import settings
from django.conf.urls.static import static

from book_store.apps.store.views import BookListView, BookCreateView, BookUpdateView, \
    BookDetailView, BookDeleteView, RequestLogListView
from book_store.users.views import register

urlpatterns = [
    path('register/', register, name='register'),
    path('login/', LoginView.as_view(template_name='store/users/login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='store/users/logout.html'), name='logout'),

    path('books/', BookListView.as_view(), name='book_list'),
    path('book/create/', BookCreateView.as_view(), name='book_create'),
    path('book/<int:pk>/detail/', BookDetailView.as_view(), name='book_detail'),
    path('book/<int:pk>/update/', BookUpdateView.as_view(), name='book_update'),
    path('book/<int:pk>/delete/', BookDeleteView.as_view(), name='book_delete'),
    path('requests/', RequestLogListView.as_view(), name='requests'),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
