from django import template
from django.conf import settings
register = template.Library()


@register.simple_tag
def copyright_years():
    return "<p>Copyright &copy; {start_year} - {end_year} <strong>Djangostars</strong>. All Rights Reserved.</p>".format(
        start_year=settings.COPYRIGHT_START_YEAR,
        end_year=settings.COPYRIGHT_END_YEAR,
    )
