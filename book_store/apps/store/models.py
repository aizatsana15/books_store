from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.timezone import now
from isbn_field import ISBNField
from PIL import Image


class Author(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super().save()


class Book(models.Model):
    image = models.ImageField(default='default_book.jpg', upload_to='media/books_pigs')
    title = models.CharField(max_length=100)
    author = models.ManyToManyField(Author)
    # isbn = ISBNField(clean_isbn=False)
    isbn = models.IntegerField('ISBN',
                               help_text='13 Character <a href="https://www.isbn-international.org/content/what-isbn'
                                         '">ISBN number</a>')
    price = models.DecimalField(decimal_places=2, max_digits=8)
    publish_date = models.DateField(default=timezone.now)
    dt_created = models.DateTimeField(default=now)

    def get_absolute_url(self):
        return reverse("book_list")

    # def get_absolute_url(self):
    #     return reverse('book_list', kwargs={'pk': self.pk})

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super().save()

        img = Image.open(self.image.path)
        # import pdb; pdb.set_trace()
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


class RequestLog(models.Model):
    method = models.CharField(max_length=10)
    host = models.CharField(max_length=20)
    path = models.CharField(max_length=100)
    query = models.CharField(max_length=300)
    time = models.DateTimeField()
