## Сontents
* [Name](#name)
* [Setup](#setup)

## Name
Book Store


## Setup  
To run this project:
```
$ git clone https://gitlab.com/aizatsana15/books_store.git
$ pip3 install -r requirements.txt
$ python3 manage.py migrate
$ python3 manage.py loaddata fixtures.json  

```
